let head_wrapper = document.querySelector(".first-slide .head_wrapper");

if (head_wrapper) {
    let head__text = head_wrapper.querySelector(".head__text");

    z1_logo_height(head_wrapper, head__text);

    window.addEventListener("resize", function() {
        z1_logo_height(head_wrapper, head__text);
    });
    window.addEventListener("transitionrun", function() { 
        z1_logo_height(head_wrapper, head__text);
    });
    document.addEventListener("DOMContentLoaded", function() {
        z1_logo_height(head_wrapper, head__text);
    });
    window.addEventListener("load", function() {
        z1_logo_height(head_wrapper, head__text);
    });
    head__text.addEventListener("change", function() {
        z1_logo_height(head_wrapper, head__text);
    });
    head__text.addEventListener("DOMSubtreeModified", function() {
        z1_logo_height(head_wrapper, head__text);
    });
} 

function z1_logo_height(object_height_set, odject_height_measure) {
    let height = odject_height_measure.scrollHeight;  
    object_height_set.style.cssText = "height:" + height + "px"; 
};