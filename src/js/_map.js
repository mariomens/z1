let maps = document.querySelector("#map");
 
if (maps) {
  let MARKER_ICON_URL = '/html/files/img/_map/icon.svg'; // <- для сервера, внутри Webpack -> '/files/img/_map/icon.svg'
  
  if (!UrlExists(MARKER_ICON_URL)) { //Проверка на наличие файла
    MARKER_ICON_URL = undefined;
  };  

  ymaps.ready(initmap);
  
  function initmap() {

    var map = new ymaps.Map("map", {
      center: [53.215548, 45.005846], 
      zoom: 17,
      controls: []
    });
    map.behaviors.disable('scrollZoom');

  // Добавляем свою метку
  var placemark = new ymaps.Placemark([53.215682, 45.006555], null, {
    iconLayout: 'default#image',
    iconImageHref: MARKER_ICON_URL,
    iconImageSize: [60, 60]
  });

  map.geoObjects.add(placemark);

  let canvas = maps.querySelector("canvas"); // Добавление прыжков для своей метки
  setTimeout( () => {
    let icon_zerone = maps.querySelector(".ymaps-2-1-78-placemark-overlay");

    if (icon_zerone) {
      setInterval(() => {
        icon_zerone.classList.toggle("active");
      }, 1000);
    }
  }, 1000);


  //Отключение перемещения карты мобильными устройствами при касании одним пальцем
  let conditionHeight = (window.innerHeight <= 576) && (window.orientation === 90);
  let conditionWidth = (window.innerWidth <= 576) && (window.orientation === 0);

  if ( conditionHeight || conditionWidth) {
    map.behaviors
    // Отключаем часть включенных по умолчанию поведений:
    //  - drag - перемещение карты при нажатой левой кнопки мыши;
    .disable(['drag', 'rightMouseButtonMagnifier'])
  } 

  window.addEventListener('resize', function(e){
    conditionHeight = (window.innerHeight <= 576) && (window.orientation === 90);
    conditionWidth = (window.innerWidth <= 576) && (window.orientation === 0);

    if ( conditionHeight || conditionWidth) {
      map.behaviors
      .disable(['drag', 'rightMouseButtonMagnifier'])
    } else {
      map.behaviors
      .enable(['drag', 'rightMouseButtonMagnifier'])
    }
  })



};

  function UrlExists(url){
    let http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
  }

}
