//import scss styles

import "../scss/main.scss";

// libs
 
import "./libs/fslightbox"; 

//import JS

import "./_header-nav";
import "./calculate_window_pos";
import "./index_first-slide";
import "./404";
import "./scroll";
import "./service";
import "./case";
import "./contact-page";
import "./contacts-popup";
import "./header";
import "./index-command-slider";
import "./vacancies";
import "./case_wrapper-handler";
import "./_map";
import "./detail__form";