let case_block = document.querySelector(".cases-deatil");
let case_list = document.querySelector(".cases-deatil__work-list");

if (case_list) { 
    document.querySelector("html").style.cssText = "scroll-behavior: smooth";
}

if (case_block) {
    let cases_desc = case_block.querySelector(".cases-description__item-text");
    let case_desc_img = cases_desc.querySelectorAll("img")
    let anchorSelector = "h3, h4, h5, h6";
    let scroll_to_top = case_block.querySelector(".cases-deatil__description-title");
    let scroll_premission;
    
    window.addEventListener("DOMContentLoaded", ()=>{
        let anchors_cases_decs = cases_desc.querySelectorAll(anchorSelector);

        case_list.addEventListener("click", function(event){ // move to anchor
            moveToAnchor(event);
        });
        
        document.addEventListener("scroll", function(){
            animateActiveAnchor(anchors_cases_decs);
        });
    });

    window.addEventListener("load", function(){
        createImgZoom(case_desc_img); 
        case_desc_img = null;
        createAnchor(anchorSelector);    // create anchor  || Расстановка якорей
    })

    scroll_to_top.addEventListener("click", ()=> {
        scroll_premission = true;
        setTimeout(()=> {scroll_premission = false}, 500)
    })

    function createImgZoom(obg_list) {
        let i = 0;
        for (const obg of obg_list) {
            obg.outerHTML = "<a href='" + obg.src + "'data-fslightbox='gallery"+ i +" '>" + obg.outerHTML + "</a>";
            i++;
        }
        refreshFsLightbox();
    }
    
    function animateActiveAnchor(objectList) {
        if (scroll_premission) return;
        
        for (let i = (objectList.length - 1); i >= 0; i--) {
            let headerAnchor = objectList[i];
            let positionAnchor = (headerAnchor.offsetTop - MISTAKE_ANCHOR_OFFSET);

            if (window_offset_top >= positionAnchor) {
                let asideAnchors = case_list.querySelectorAll("a.cases-deatil__work-link");
                let asideScrollInMenu;

                for (const asideAnchorItem of asideAnchors) {
                    let headerText = headerAnchor.innerText.toUpperCase();
                    let asideAnchorText = asideAnchorItem.innerText.replace(/^[\d][\s]/g, ""); 
                    asideAnchorItem.classList.remove("active"); 

                    if (headerText === asideAnchorText) {
                        asideAnchorItem.classList.add("active");
                        asideScrollInMenu = (asideAnchorItem.offsetTop - case_list.offsetTop) > 20? //скролл бокового меню
                                            (asideAnchorItem.offsetTop - case_list.offsetTop) : 0;
                        case_list.scrollTo(0,asideScrollInMenu);
                    }
                } 
                break;
            }
        }
    }
}

// scroll aside panel // Зависит от calculate_window_pos.js
let case_head = document.querySelector(".cases-deatil__head");

if (case_head) { // Move aside panel перемещение боковой панел
    let aside_panel = case_head.querySelector(".cases-deatil__description");
    document.addEventListener("scroll", function(){
        let case_height = case_head.offsetHeight ;
        let aside_position = getComputedStyle(aside_panel).position === "fixed";
        if ( (pageYOffset > case_height) && (aside_position) ) {
            aside_panel.style.cssText = "top:" + (header_height + 20)+ "px"; 
        } else {
            aside_panel.style.cssText = "";
        }
    })
}

//создание якорей
function createAnchor(selector) {
    let cases__text = document.querySelector(".cases-description__item-text");
    let str_RegExp = "";
    let cases_headers = cases__text.querySelectorAll(selector); // seach title for anchor ||заголовки для установки якорей
    cases_headers = Array.from(cases_headers);

    case_list.innerHTML = "";
   
    cases_headers.forEach(function(item, i, arr)  {
        let cases_header = arr[i];

        let li_anchor = document.createElement("li");
        li_anchor.classList.add("cases-deatil__work-item");
        let a_anchor = document.createElement("a");
        
        if (i === 0) {
            a_anchor.classList.add("active");
        }
        a_anchor.href = "#anchor" + (i + 1);
        a_anchor.classList.add("cases-deatil__work-link");
        a_anchor.innerHTML = (i + 1) + " " + cases_header.innerHTML;
        
        li_anchor.append(a_anchor);
        case_list.append(li_anchor)

        let anchor = document.createElement("a");
        anchor.name = "anchor" + (i + 1);
        anchor.classList.add("anchor");
        cases_header.append(anchor);
    });

    //first_case_link = null;
    str_RegExp = null;
    cases_headers = null;
    cases__text = null;
}

// Зависит от calculate_window_pos.js
function moveToAnchor(event) {
    let case_list_link = case_list.querySelector(".cases-deatil__work-link.active");

    if (event.target === case_list) {
        return false;
    }

    case_list_link.classList.remove("active");
    event.target.classList.add("active");
    event.preventDefault();

    let link_anchor = event.target.hash;
    link_anchor = link_anchor.replace("#", "");

    let case_items = document.querySelectorAll(".cases-description__item");

    for (const case_item of case_items) {
        let item_anchor = case_item.querySelector("a[name='" + link_anchor + "']");
        if (item_anchor) {
            let y = positionObject.getPosition(item_anchor);
            y = item_anchor.offsetTop;
            scrollToAnchor(y);
            break;
        }
    }
    case_list_link = null;
    case_items = null;
}

const MISTAKE_ANCHOR_OFFSET = 50;
// Зависит от calculate_window_pos.js
function scrollToAnchor(y) {
    y = y - header_height - MISTAKE_ANCHOR_OFFSET;
    window.scrollTo(0,y);
}

