/* 
    Этот файл считает положение окна с учетом шапки сверху
    от этого файла зависят scroll.js, header.js, case.js
*/
window.positionObject = {};

window.header = document.querySelector("header");
window.header_height;

window.window_height; // Высота окна
window.window_width;
window.window_offset_top; // Положение верхней точки окна
window.window_offset_bottom; // Положение нижней точки окна

window.addEventListener("resize", function(){
    calculateWindowPosition();
})

window.addEventListener("scroll", function(){
    calculateWindowPosition();
})

window.addEventListener("DOMContentLoaded", function(){
    calculateWindowPosition();
})

function calculateWindowPosition() {
    header_height = header.offsetHeight;
    window_height = window.innerHeight - header_height; // Высота окна
    window_offset_top = window.pageYOffset + header_height; // Положение верхней точки окна
    window_offset_bottom = window_offset_top + window_height; // Положение нижней точки окна
}

window.positionObject.getPosition = function (el) {
    var y = 0;
    y = y + el.offsetTop;
    while ( (el.tagName !== "BODY")) {
        el = el.parentElement; 
        y = y + el.offsetTop;
    }
    return y;
};

const OFFSET_ERROR = 0.25;

window.positionObject.blockInCenter = function(startY, endY, heightY) {
    let window_center_top = window_offset_top + window_height*(0.5 - OFFSET_ERROR);
    let window_center_bottom = window_offset_top + window_height*(0.5 + OFFSET_ERROR);

    let block_center_top = startY + heightY*(0.5 - OFFSET_ERROR);
    let bloc_window_center = startY + window_height * (0.5 - OFFSET_ERROR);
    
    if ( (block_center_top <= window_center_top) && (block_center_top <= window_center_bottom)) { 
        return true;
    } else if (bloc_window_center <= window_center_bottom) {
        return true;
    } else { 
        return false;
    }
}