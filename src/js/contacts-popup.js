let connectUsBtns = document.querySelectorAll(".head__connection-link");

if (connectUsBtns) {
    let connectUsPopup = document.querySelector(".contacts-us-popup");

    for (const connectUs of connectUsBtns) {
        connectUs.addEventListener("click", ()=>{
            connectUsPopup.classList.add("active");
        });
    }

    let contactPopupOverlay = connectUsPopup.querySelector(".contacts-us-popup_overlay");
    let formCloseBtns = connectUsPopup.querySelectorAll(".contact-form__close-button");

    contactPopupOverlay.addEventListener("click", () => {
        connectUsPopup.classList.remove("active");
        if (connectUsPopup.classList.contains("success")) {
            connectUsPopup.classList.remove("success");
        }
    } ); 

    for (const closeBtn of formCloseBtns) {
        closeBtn.addEventListener("click", () => {
            connectUsPopup.classList.remove("active");
            if (connectUsPopup.classList.contains("success")) {
                connectUsPopup.classList.remove("success");
            }
        } );         
    }

    let contactForm = connectUsPopup.querySelector(".contact-form");
    let personalData = contactForm.querySelector(".user-agree");
    let personalPopup = connectUsPopup.querySelector(".personal-popup");
    let personalPopupOverlay = connectUsPopup.querySelector(".personal-popup__overlay");
    let personalCloseBut = connectUsPopup.querySelector(".personal-popup__close");
    let checkBoxDataStatus = false;

    personalData.addEventListener("click", (event) => {
        event.preventDefault();
        setTimeout( ()=>{
            if (!checkBoxDataStatus) {
            personalPopup.classList.add("active");
            checkBoxDataStatus = true;
            } else {
                checkBoxDataStatus = false;
            }
        }, 500);
    } );

    personalPopupOverlay.addEventListener("click", () => {
        personalPopup.classList.remove("active");
    } );

    personalCloseBut.addEventListener("click", () => {
        personalPopup.classList.remove("active");
    } );

}