// Зависит от calculate_window_pos.js

window.addEventListener("scroll", function(){
    setTimeout(headerColor, 200);
})
window.addEventListener("DOMContentLoaded", function(){
    setTimeout(headerColor, 200);
})
window.addEventListener("resize", function(){
    setTimeout(headerColor, 200);
})
function headerColor() {
    if (window_offset_top > window_height*0.15) {
        header.classList.add("active");
        return;
    } else if (window_offset_top <= window_height*0.15) {
        header.classList.remove("active");
    }
}

//lang toggler

let langs = header.querySelectorAll(".lang-list__link");

for (const langLink of langs) {
    langLink.addEventListener("click", changeActiveLink);
}

function changeActiveLink() {
    let actualLang = this.dataset.lang;
    for (const langLink of langs) {
        let langLinkLang = langLink.dataset.lang;

        if (langLinkLang === actualLang) {
            langLink.classList.add("active");
        } else {
            langLink.classList.remove("active");
        }
    }
}