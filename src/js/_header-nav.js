//Бургер меню
let nav = document.querySelector(".navigation");
let burger_buts = nav.querySelectorAll(".burger-button");
var is_menu_open = false;

for (const but of burger_buts) {
    but.addEventListener("click", () => {
        nav.classList.toggle("active");
        is_menu_open = nav.classList.contains("active"); 
    })
}

document.addEventListener("mouseup", function(e) { 

    if (is_menu_open) {
        let arr = e.target.classList.value.split(" "); 
        arr[0] = "." + arr[0];
        let class_name = arr.join("."); 

        let selected = nav.querySelector(class_name);   

        if ( (e.target !== nav) && (selected == null ) ) { 
            nav.classList.remove("active");
            is_menu_open = false;
        }
    } else {
        return;
    }
}, false);