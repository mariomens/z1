// Зависит от calculate_window_pos.js
let index = document.querySelector(".index")?.classList.contains("index");

if (index) {
    let titles = document.querySelectorAll(".section-title"); //Заголовкам с эти классом делает эффект подчеркивания

    let animation_bot_objects = document.querySelectorAll(".animation-bottom");
    let animation_left_right_objects = document.querySelectorAll(".animation-right, .animation-left, .animation-opacity");

    let callUsParent = document.querySelector(".index").querySelector(".head__connection");
    let callUs = callUsParent.querySelector(".head__connection-link");
    let callUsPaste = header.querySelector(".head__connection");

    var objHeight = callUs.clientHeight;

    window.addEventListener("scroll", function(){
        title_inderline(titles);
        animationBottom(animation_bot_objects);
        animationRightLeft(animation_left_right_objects);
        vievCallUs(callUs,callUsParent,callUsPaste,event);
    })

    window.addEventListener("DOMContentLoaded", function(){
        initTitleUnderLine(titles);
        animationBottom(animation_bot_objects);
        animationRightLeft(animation_left_right_objects);
        vievCallUs(callUs,callUsParent,callUsPaste,event);
    })
    window.addEventListener("resize", function(){
        vievCallUs(callUs,callUsParent,callUsPaste,event);
    })

    function initTitleUnderLine(head_arr) {
        for (const titleHead of head_arr) {
            let headFontSize = getComputedStyle(titleHead).fontSize.replace(/px/g, "");
            headFontSize = Number(headFontSize);
            let headHeight =  titleHead.clientHeight;
            
            if ( (headHeight/headFontSize) < 1.5 ) { //Одна строчка
                let createSpan = document.createElement("span");
                createSpan.innerHTML = titleHead.innerHTML;
                let createSpanTitle = document.createElement("span");
                createSpanTitle.classList.add("title-line");
                createSpan.append(createSpanTitle);
                titleHead.innerHTML = "";
                titleHead.append(createSpan);
            } else {    //Более строк
                let titleString = titleHead.innerHTML.trim().replace(/\s\s/g, "");
                let titleStringArr = titleString.split("<br>");
                for (let i=0; i < titleStringArr.length; i++) {
                    let createSpan = document.createElement("span");
                    createSpan.innerHTML = titleStringArr[i];
                    let createSpanTitle = document.createElement("span");
                    createSpanTitle.classList.add("title-line");
                    createSpan.append(createSpanTitle);

                    titleStringArr[i] = ""
                    titleStringArr[i] = createSpan.outerHTML;
                }
                let htmlHead = titleStringArr.join("");
                titleHead.innerHTML = "";
                titleHead.innerHTML = htmlHead;
            }
        }
    }

    function title_inderline(head_arr) {
        for (const head of head_arr) {
            let point_start = positionObject.getPosition(head);
            let parentHeight = findParentHeight(head);
            let point_end = point_start + parentHeight; 

            if (positionObject.blockInCenter(point_start, point_end, parentHeight)) {
                let lines = head.querySelectorAll(".title-line");
                for (const line of lines) {
                    line.classList.add("active");
                }
            }

        }
    }
}
function findParentHeight(el) {
    el = el.parentElement;

    if (el.tagName !== "SECTION") {
        return findParentHeight(el);
    } else {   
        return (el.offsetHeight);
    }
}

function vievCallUs(obj, objInitialParent, objPasteParent,event) {
    let obgPosition = positionObject.getPosition(obj) - obj.clientHeight;
    let objInitialPosition = positionObject.getPosition(objInitialParent) - objInitialParent.clientHeight;

    let eventType = event ? event.type : null;
/*
    if (eventType === "resize") {
        callUsResizeMove()
        return;
    } */
    if (eventType === "DOMContentLoaded") {
        objPasteParent.innerHTML ="";
        if (window_offset_top >= obgPosition) {
            objPasteParent.append(obj);
            obj.classList.add("move-header");
        } 
        return;
    }
//    console.log(window_offset_top + " " + header_height + "  " + objInitialPosition);

    if ( !obj.classList.contains("complite") ) {
        if (window_offset_top >= obgPosition*0.99) {
            callUsToEndPosition();
            return;
        } 

    } else if (window_offset_top <= objInitialPosition*0.9) {
        callUsToStarPosition();
    }

    function callUsToEndPosition () {

        if (!obj.classList.contains("move-header")) {
            objHeight = obj.clientHeight;
            objPasteParent.append(obj);
            obj.classList.add("move-header");
        }
        //objInitialPosition = positionObject.getPosition(objInitialParent) - 1.0*obj.offsetHeight;

        let topCoordinate = - window_offset_top + header_height  + objInitialPosition;
        //debugger;
   
        obj.style.cssText = "position: fixed !important; top:" + topCoordinate + "px" + 
        "; z-index: 1000; left: 50%; transform: translateX(-50%); height:" +
        objHeight + "px; z-index: 1000;";

        if (topCoordinate < 3) {
            obj.style.cssText = "";
            obj.classList.add("complite");
        }

        if (topCoordinate > header_height*3) {
            obj.style.cssText = "";
            callUsToStarPosition();
        }
  

    }

    function callUsToStarPosition() {
        obj.style.cssText = "";
        objInitialParent.append(obj);
        obj.classList.remove("move-header")
        obj.classList.remove("complite")
    }
}

function animationBottom(obj_list) {
    for (const obj of obj_list) {
        let top_coordinate =Number( positionObject.getPosition(obj) - 50 );

        if (window_offset_bottom >= top_coordinate) {
            obj.classList.add("ready");
        } else {
            obj.classList.remove("ready");
        }
    }
}

function animationRightLeft(obj_list) {
    for (const obj of obj_list) {
        let top_coordinate = positionObject.getPosition(obj) + 50;
        let scrollHalfContent = window_offset_bottom > (top_coordinate - (obj.offsetHeight));

        if (window_offset_bottom >= top_coordinate) {
            obj.classList.add("ready");
        } else if(scrollHalfContent) {
            obj.classList.add("ready");
        } else {
            obj.classList.remove("ready");
        }
    }
}
