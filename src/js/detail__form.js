
let detail__form = document.querySelector('.case__detail__form');

if(detail__form) {
    let politic_btn = detail__form.querySelector('.case__detail__politics__agree');
    let politic__popup = detail__form.querySelector('.personal-popup');
    let overlay__close = detail__form.querySelector('.personal-popup__overlay')

    politic_btn.addEventListener('click', open__agree__politic__event);


    function open__agree__politic__event() {
        setTimeout( ()=>{
            politic__popup.classList.add('active');
        }, 500);
    }

    let close__case__detail__btn = detail__form.querySelector('.personal-popup__close');

    close__case__detail__btn.addEventListener('click', close__case__detail__event);

    function close__case__detail__event() {
        politic__popup.classList.remove('active');
    }

    overlay__close.addEventListener('click', overlay__close__event);

    function overlay__close__event() {
        politic__popup.classList.remove('active');
    }
}

