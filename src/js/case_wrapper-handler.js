let cases = document.querySelector(".cases");
//На мобильных устройствах Блокирует переход по ссылке при одиночном click, если два fast-click за время TIME_DOUBLE_CLICK, то разрешает переход
if ( (cases !== null) && (cases !== undefined) ) { 
    let caseWrap = cases.querySelectorAll(".case_wrapper");
    let oldTime;
    const TIME_DOUBLE_CLICK = 200;  // интервал между двумя касаниями

    for (const caseElem of caseWrap) {
        caseElem.addEventListener("click", function(event) {
            caseHandler(event, this);
        }, false);
    }
    
    function caseHandler(event, elem) {
        let conditionHeight = (window.innerHeight > 576) && (window.orientation === undefined);
        let conditionWidth =  (window.innerWidth  > 576) && (window.orientation === undefined); //на ПК window.orientation === undefined

        if ( conditionHeight || conditionWidth) {
            for (const caseItem of caseWrap) {
                caseItem.classList.remove("active");
            }    
            return;
        };

        let nowTime = new Date().getTime();
        let timeDoubleClick = nowTime - oldTime;

        if ( (timeDoubleClick > TIME_DOUBLE_CLICK) || isNaN(timeDoubleClick) ) {
            event.preventDefault();
        }
        elem.classList.toggle("active");       
        oldTime = new Date().getTime();
    }
}