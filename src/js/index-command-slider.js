import Glide,{ Controls, Breakpoints }  from './libs/glide';
// index blog_slider
let index = document.querySelector(".index");
let command = document.querySelector(".command");


if (index || command) {
    const NUMBER_OF_SLIDER = index? 1.3 : 1;
    let blog = document.querySelector(".blog, .photo");
    let blog_slider = blog.querySelector(".blog__cases-wrapper");
    let orientation_flag = false;

     window.addEventListener("orientationchange", function() {
        initBlogSlider(blog);
        orientation_flag = true;
        //console.log("orientationchange");
    });
    window.addEventListener("resize", function() {
        if (!orientation_flag) {
        sliderPermission();
        initBlogSlider(blog);
        //console.log("resize");
        }
        orientation_flag = false;
    }); 
    document.addEventListener("DOMContentLoaded", function() {
        initBlogSlider(blog);
    });


    let BlogGlide, permission,
        blogCounter = 0,
        blogActive = false;

    sliderPermission();

    function initBlogSlider(obj) {
        
        if ( (permission) && (window.orientation === 0) && (!blogActive) ) { 
            BlogGlide = new Glide(obj, {
                breakpoints: {
                    576: {
                      perView: 1,
                      type: "slider",
                      focusAt: 0,
                      perView: NUMBER_OF_SLIDER,
                      rewind: true,
                      //autoplay: 3000,
                      swipeThreshold: 40,
                      bound: true,
                      animationDuration: 300,
                    }
                  }
              }).mount();
              blogCounter++;
              blogActive = true;
              BlogGlide.enable();
              //console.log("поставил слайдер");
              return;
        } else if ( (permission) && (window.orientation != 0) && (blogCounter > 0) && (blogActive)) {
            sliderDestroy();
            return;
        } else if ( (!permission) && (blogCounter > 0)  && (blogActive) && (window.orientation === 0)) {
            sliderDestroy();
            return;
        }
    }

    function sliderDestroy() {
        //console.log("сломал слайдер");
        BlogGlide.disable();
        setTimeout(()=> {
            BlogGlide.destroy(); 
            blogActive = false;
        } , 100) 
    }

    function sliderPermission() { 
        if ( ((window.innerWidth < 576) && (window.orientation === 0)) ||
        ((window.innerHeight < 576) && (window.orientation === 90))) {
       permission = true;
        } else {
            permission = false;
        } 
    }
}

if (command) {
    let photoItems = command.querySelectorAll('.photo__item');

    for (const photo of photoItems) {
        photo.addEventListener("click", function(){
            photoItemActiveTogle(this)
        });
    }

    function photoItemActiveTogle(photo__item){
        if ( (window.innerHeight <= 576) || (window.innerWidth <= 576)) {
            photo__item.classList.toggle("click-active");
        } else {
            for (const photo of photoItems) {
                photo.classList.remove("active");
            }
        }
    }
}