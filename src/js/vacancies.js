
let vacancies = document.querySelector('.vacancies')

if(vacancies != null) {
    let popup_modal = document.querySelector('.vacancies__popup');
    // Change input name with chose files
    let input_file = document.querySelector('.input__file');
    let label = document.querySelector('.input__file-button-text');

    input_file.addEventListener('change', get_file);

    function get_file() {
        let file_name = this.files[0].name;
        label.innerHTML = file_name;
    }


    // Delete active class
    function delete_active_event() {
        if(popup_modal.classList.contains('active')) {
            popup_modal.classList.remove('active');
        }
    }
    // Delete agreement active
    let agreement_information = popup_modal.querySelector('.personal-popup');
    function close_agreement_function() {
        if(agreement_information.classList.contains('active')) {
            agreement_information.classList.remove('active');
        }
    }

    // Open Modal Event
    let open_modal = document.getElementsByClassName('vacancies__open__modal');

    for(let i = 0; i < open_modal.length; i++) {
        open_modal[i].addEventListener('click', open_modal_event)
    }

    function open_modal_event() {
        if(popup_modal.classList.contains('active')) {
            popup_modal.classList.remove('active');
        } else {
            popup_modal.classList.add('active');
        }

        let hidden__input_name = document.querySelector('.vacancies__name')
        let parent_block = this.closest('.vacancies__description__content__parent');
        let vacancy_name = parent_block.querySelector('h4');
        hidden__input_name.value = vacancy_name.innerText;

    }



    // Close Modal Event
    let close_modal = popup_modal.getElementsByClassName('contact-form__close-button');

    for(let i = 0; i < close_modal.length; i++) {
        close_modal[i].addEventListener('click', close_modal_event);
    }

    function close_modal_event() {
        delete_active_event()
    }



    // Overlay Close

    let close_overlay = popup_modal.getElementsByClassName('contacts-us-popup_overlay');

    for(let i = 0; i < close_overlay.length; i++) {
        close_overlay[i].addEventListener('click', close_overlay_event);
    }

    function close_overlay_event() {
        delete_active_event();
    }


    let personal_data = popup_modal.querySelector(".user-agree");
    personal_data.addEventListener('click', personal_data_event);

    function personal_data_event() {
        setTimeout( ()=>{
            if(agreement_information.classList.contains('active')) {
                agreement_information.classList.remove('active');
            } else {
                agreement_information.classList.add('active');
            }
        }, 500);
    }



    let close_agreement_information = popup_modal.querySelector('.personal-popup__close');
    close_agreement_information.addEventListener('click', close_agreement_information_event);

    function close_agreement_information_event() {
        close_agreement_function();
    }



    //Description changer

    let description__changer__btn = document.getElementsByClassName('vacancies__list__link');
    let description__content = document.getElementsByClassName('vacancies__description__content__parent');

    for (let i = 0; i < description__changer__btn.length; i++) {
        description__changer__btn[i].addEventListener('click', description__changer__event)
    }

    function description__changer__event() {
        let current_elem = this.dataset.name.trim()
        remove_active_event()
        for (let i = 0; i < description__content.length; i++) {
            description__content[i].classList.remove('active');
            description__content[i].classList.remove('none');
            description__show__all_btn.classList.remove('active');



            if (description__content[i].dataset.name.trim() === current_elem) {
                description__content[i].classList.add('active');
                this.classList.add('active');
            } else {
                description__content[i].classList.add('none');
            }
        }

    }

    let description__show__all_btn = document.querySelector('.vacancies__list__link_all');

    description__show__all_btn.addEventListener('click', description__show__all_event);

    function description__show__all_event() {
        for (let i = 0; i < description__content.length; i++) {
            description__content[i].classList.remove('active')
            description__content[i].classList.remove('none');
            remove_active_event();
            this.classList.add('active');
        }
    }

    function remove_active_event() {
        for (let i = 0; i < description__changer__btn.length; i++) {
            if(description__changer__btn[i].classList.contains('active')) {
                description__changer__btn[i].classList.remove('active')
            }
        }

    }
}

