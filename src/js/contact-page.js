let contacts = document.querySelector(".contacts");
if (contacts) {
    let contactForm = contacts.querySelector(".contact-form");
    let personalData = contactForm.querySelector(".user-agree");
    let personalPopup = contactForm.querySelector(".personal-popup");
    let personalPopupOverlay = contactForm.querySelector(".personal-popup__overlay");
    let personalCloseBut = contactForm.querySelector(".personal-popup__close");
    let checkBoxDataStatus = false;

    personalData.addEventListener("click", (event) => {
        event.preventDefault();
        setTimeout( ()=>{
            if (!checkBoxDataStatus) {
            personalPopup.classList.add("active");
            checkBoxDataStatus = true;
            } else {
                checkBoxDataStatus = false;
            }
        }, 1000);
    } );
    personalPopupOverlay.addEventListener("click", () => {
        personalPopup.classList.remove("active");
    } );
    personalCloseBut.addEventListener("click", () => {
        personalPopup.classList.remove("active");
    } );

    let connection_link = header.querySelector(".head__connection-link");
    connection_link.style.cssText= "display:none";
}
window.addEventListener("DOMContentLoaded", function() {
    function setCursorPosition(pos, elem) {
        elem.focus();

        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }
function mask(event) {
    var matrix = "+7 (___) ___ ____",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");

    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function(a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });

    if (event.type == "blur") {
        
        if (this.value.length == 2) this.value = ""
    } else setCursorPosition(this.value.length, this)

};

    var input = document.querySelectorAll("#tel");
    for (let i = 0; i < input.length; i++) {
        input[i].addEventListener("input", mask, false);
        input[i].addEventListener("focus", mask, false);
        input[i].addEventListener("blur", mask, false);
    }
});

let case__form__popup = document.querySelector('.case__detail__form');

if(case__form__popup !== null) {
    let checkbox_detail = case__form__popup.querySelector('.toggle__input');
    checkbox_detail.addEventListener('click',  checkbox_detail_event);

    function checkbox_detail_event() {
        let submit = case__form__popup.querySelector('.case__submit');

        if(this.checked === true) {
            submit.disabled = false;
        } else {
            submit.disabled = true;
        }
    }
}
