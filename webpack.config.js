const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin'); 
const autoprefixer = require('autoprefixer');

module.exports = {
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    main: ["@babel/polyfill", "./js/index.js"],
    // img: './img/',
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },

  resolve: {
    extensions: [".js", ".json", ".png", "svg", "gif", "jpeg", "jpg", "cur", "pptx"],
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@models": path.resolve(__dirname, "src/models"),
    },
  },
  devServer: {
    port: 4300,
  },
  plugins: [
    new HTMLWebpackPlugin({ 
      template: "./index.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "404.html",
      template: "./404.html",
      minify: false,
    }), 
    new HTMLWebpackPlugin({
      filename: "command.html",
      template: "./command.html",
      minify: false,
    }),  
    new HTMLWebpackPlugin({
      filename: "contacts.html",
      template: "./contacts.html",
      minify: false,
    }), 
    new HTMLWebpackPlugin({
      filename: "service.html",
      template: "./service.html",
      minify: false,
    }),  
    new HTMLWebpackPlugin({
      filename: "portfolio.html",
      template: "./portfolio.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "blog.html",
      template: "./blog.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "case.html",
      template: "./case.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "blog_item.html",
      template: "./blog_item.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "vacancies.html",
      template: "./vacancies.html",
      minify: false,
    }),
     /*
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
*/
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].bundle.css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: "files", to: "files" }, 
        { from: "./favicon.ico", to: "./favicon.ico" }, 
      ],
    }),
    
  ],
  module: {
    rules: [ 
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env']
            }
        }
    },

      {
        test: /\.hbs$/,                           //для обработки шаблонов
        include: path.resolve(__dirname, "src"), // old_ver include: path.resolve(__dirname, "src/template"),
        loader: "html-loader",
        options: {
          // Disables attributes processing
          minimize: false,
        }
      },

      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader","postcss-loader"],
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader",  'postcss-loader' ,  "less-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true, },},  
          'postcss-loader' ,    
          { loader: "sass-loader", options: { sourceMap: true, },},
      ],
      }, 
      {
        test: /\.(png|svg|gif|jpg|jpeg|cur|pptx)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
    ],
  },
};
